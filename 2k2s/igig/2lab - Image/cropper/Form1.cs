﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cropper
{
    public partial class Form1 : Form
    {
        public Points firstP = new Points();
        public Points secondP = new Points();
        public Points controller = new Points();
        public Bitmap cloneBitmap = new Bitmap(1, 1);

        int width = 0;
        int height = 0;
        int i = 0;

        public Form1()
        {

            InitializeComponent();
            controller.clearLabels(this);
            originImg.SizeMode = PictureBoxSizeMode.AutoSize;

        }

        private void originImg_Click(object sender, EventArgs e)
        {

            if(i == 0)
            {
                Point first = new Point();
                firstP.x = originImg.PointToClient(Cursor.Position).X;
                firstP.y = originImg.PointToClient(Cursor.Position).Y;
                flx.Text = "x:";
                fly.Text = "y:";
                flx.Text = flx.Text + firstP.x;
                fly.Text = fly.Text + firstP.y;
                i = 1;
            } else
            {
                secondP.x = originImg.PointToClient(Cursor.Position).X;
                secondP.y = originImg.PointToClient(Cursor.Position).Y;
                slx.Text = "x:";
                sly.Text = "y:";
                slx.Text = slx.Text + secondP.x;
                sly.Text = sly.Text + secondP.y;
                i = 0;
            }

            if(firstP.x != 0 && secondP.x != 0)
            {
                if(true)
                {
                    width = Math.Abs(secondP.x - firstP.x);
                } 

                if(true)
                {
                    height = Math.Abs(secondP.y - firstP.y);
                } 

                Bitmap origin = new Bitmap(originImg.Image);
                
                Rectangle cloneRect = new Rectangle(firstP.x, firstP.y, width, height);
                System.Drawing.Imaging.PixelFormat format = origin.PixelFormat;
                cloneBitmap = origin.Clone(cloneRect, format);
                cloneBitmap.Save(width+"x"+height+".png");

                imgBox imgbox = new imgBox(this);

                //originImg.Paint += new PaintEventHandler(pictureBox_Paint);
                imgbox.pictureBox1.Width = width;
                imgbox.pictureBox1.Height = height;
                imgbox.Width = imgbox.pictureBox1.Width + 15;
                imgbox.Height = imgbox.pictureBox1.Height + 40;
                //imgbox.heightText.Location = new Point(5,imgbox.Height);
                imgbox.heightText.Width = 80;
                imgbox.heightText.Text = width + " x" + height + "px";
                imgbox.Show();
                imgbox.pictureBox1.Image = Image.FromHbitmap(cloneBitmap.GetHbitmap());
            }

        }

        private void loadBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "images| *.JPG; *.PNG; *.GJF";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                originImg.Image = Image.FromFile(ofd.FileName);
            }
        }

        private void showSize() //debug
        {
            MessageBox.Show(originImg.Size.ToString());
            MessageBox.Show(Cursor.Position.ToString());
        }

        private void resetBtn_Click(object sender, EventArgs e)
        {

            
        }

        private void reset()
        {
            i = 0;
            originImg.Image = null;
            firstP = Points.nullIt(firstP);
            secondP = Points.nullIt(secondP);
            controller.clearLabels(this, 1);
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            Rectangle ee = new Rectangle(firstP.x, firstP.y, width, height);

            using (Pen pen = new Pen(Color.Red, 2))
            {
                e.Graphics.DrawRectangle(pen, ee);
            }
            //MessageBox.Show("kek");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void loadOld_Click(object sender, EventArgs e)
        {
            this.reset();
            originImg.Image = cloneBitmap;
        }
    }
}
