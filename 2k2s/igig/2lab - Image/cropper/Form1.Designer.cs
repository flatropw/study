﻿namespace cropper
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.originImg = new System.Windows.Forms.PictureBox();
            this.loadBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.flx = new System.Windows.Forms.Label();
            this.fly = new System.Windows.Forms.Label();
            this.slx = new System.Windows.Forms.Label();
            this.sly = new System.Windows.Forms.Label();
            this.resetBtn = new System.Windows.Forms.Button();
            this.loadOld = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.originImg)).BeginInit();
            this.SuspendLayout();
            // 
            // originImg
            // 
            this.originImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.originImg.Location = new System.Drawing.Point(-3, -3);
            this.originImg.Name = "originImg";
            this.originImg.Size = new System.Drawing.Size(1100, 599);
            this.originImg.TabIndex = 0;
            this.originImg.TabStop = false;
            this.originImg.Click += new System.EventHandler(this.originImg_Click);
            // 
            // loadBtn
            // 
            this.loadBtn.Location = new System.Drawing.Point(1114, 12);
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(133, 63);
            this.loadBtn.TabIndex = 1;
            this.loadBtn.Text = "Load image";
            this.loadBtn.UseVisualStyleBackColor = true;
            this.loadBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1109, 220);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "First Point";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1109, 333);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Second point";
            // 
            // flx
            // 
            this.flx.AutoSize = true;
            this.flx.Location = new System.Drawing.Point(1109, 262);
            this.flx.Name = "flx";
            this.flx.Size = new System.Drawing.Size(29, 25);
            this.flx.TabIndex = 4;
            this.flx.Text = "x:";
            // 
            // fly
            // 
            this.fly.AutoSize = true;
            this.fly.Location = new System.Drawing.Point(1109, 287);
            this.fly.Name = "fly";
            this.fly.Size = new System.Drawing.Size(29, 25);
            this.fly.TabIndex = 5;
            this.fly.Text = "y:";
            // 
            // slx
            // 
            this.slx.AutoSize = true;
            this.slx.Location = new System.Drawing.Point(1109, 384);
            this.slx.Name = "slx";
            this.slx.Size = new System.Drawing.Size(29, 25);
            this.slx.TabIndex = 6;
            this.slx.Text = "x:";
            // 
            // sly
            // 
            this.sly.AutoSize = true;
            this.sly.Location = new System.Drawing.Point(1109, 409);
            this.sly.Name = "sly";
            this.sly.Size = new System.Drawing.Size(29, 25);
            this.sly.TabIndex = 7;
            this.sly.Text = "y:";
            // 
            // resetBtn
            // 
            this.resetBtn.Location = new System.Drawing.Point(1114, 154);
            this.resetBtn.Name = "resetBtn";
            this.resetBtn.Size = new System.Drawing.Size(180, 63);
            this.resetBtn.TabIndex = 8;
            this.resetBtn.Text = "Reset";
            this.resetBtn.UseVisualStyleBackColor = true;
            this.resetBtn.Click += new System.EventHandler(this.resetBtn_Click);
            // 
            // loadOld
            // 
            this.loadOld.Location = new System.Drawing.Point(1253, 12);
            this.loadOld.Name = "loadOld";
            this.loadOld.Size = new System.Drawing.Size(65, 63);
            this.loadOld.TabIndex = 9;
            this.loadOld.Text = "Old";
            this.loadOld.UseVisualStyleBackColor = true;
            this.loadOld.Click += new System.EventHandler(this.loadOld_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1341, 587);
            this.Controls.Add(this.loadOld);
            this.Controls.Add(this.resetBtn);
            this.Controls.Add(this.sly);
            this.Controls.Add(this.slx);
            this.Controls.Add(this.fly);
            this.Controls.Add(this.flx);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.loadBtn);
            this.Controls.Add(this.originImg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "www";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.originImg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox originImg;
        private System.Windows.Forms.Button loadBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button resetBtn;
        public System.Windows.Forms.Label flx;
        public System.Windows.Forms.Label fly;
        public System.Windows.Forms.Label slx;
        public System.Windows.Forms.Label sly;
        private System.Windows.Forms.Button loadOld;
    }
}

